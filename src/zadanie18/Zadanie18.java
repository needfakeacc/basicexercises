package zadanie18;

import java.util.Scanner;

public class Zadanie18 {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj ilosc liczb:");
        int iloscLiczb = scanner.nextInt();
        int max = Integer.MIN_VALUE;
        int min = Integer.MAX_VALUE;
        double sum = 0;

        for (int i = 0; i < iloscLiczb; i++) {
            int liczba = scanner.nextInt();
            if (liczba > max) {
                max = liczba;
            }
            if (liczba < min) {
                min = liczba;
            }
            sum += (double)liczba;
        }
        double srednia = sum / (double)iloscLiczb;
        System.out.println("MIN + MAX = " + (min + max));
        System.out.println("Srednia = " + srednia);
    }
}
