package zadanie3;

public class Zadanie3 {
    public static void main(String[] args) {

        int a = 2;
        int b = 3;
        int c = 4;
        int tymczasowa = a;

        a = b;
        b = c;
        c = tymczasowa;

        System.out.println("a=" + a + " b=" + b + " c=" + c);
    }
}
