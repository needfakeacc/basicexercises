package zadanie12;

import java.util.Scanner;

public class Zadanie12 {
    public static void main(String[] args) {
        Scanner laser = new Scanner(System.in);
        System.out.println("podaj liczbałkę całkowitą:");

        int liczbeł = 0;
        int i;

        try {                                  //program A
            liczbeł = laser.nextInt();
        } catch (Exception e) {
            System.out.println("To nie jest liczbeł całkowity!");
            return;
        }
        if (liczbeł <= 0) {
            System.out.println("Podaj liczbałkę dodatnią.");
        } else {
            for (i = 0; i <= liczbeł; i++) {
                if (i % 2 == 1) {
                    System.out.println(i);
                }
            }
        }
        for (i = 3; i < 100; i++) {            //program B
            if (i % 3 == 0 || i % 5 == 0) {
                System.out.println(i);
            }
        }
        int zakresDol = laser.nextInt();  //program C
        int zakresGorny = laser.nextInt();
        for (i = zakresDol; i < zakresGorny; i++) {
            if (i % 6 == 0) {
                System.out.println(i);
            }
        }
    }
}
