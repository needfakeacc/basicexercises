package zadanie13;

import java.util.Scanner;

public class Zadanie13 {
    public static void main(String[] args) {

        Scanner laser = new Scanner(System.in);
        System.out.println("Podaj wspolczynnik A:");
        int a = laser.nextInt();
        System.out.println("Podaj wspolczynnik B:");
        int b = laser.nextInt();


//        while () {
//
//            if (a < b) {
//                break;
//            } else if(a > b) {
//                System.out.println("B musi być większe od A! Podaj prawidlowy wspolczynnik B:");
//                b = laser.nextInt();
//            }
//        }

        int sum = 0;
        for (int i = a; i <= b; i++) {
            sum += i;
        }
        System.out.println("Suma = " + sum);

        int i = a;
        int sum2 = 0;
        while (i <= b) {
            sum2 += i;
            i++;
        }
        System.out.println("Suma2 = " + sum2);
    }
}
