package zadanie10;

import java.util.Scanner;

public class Zadanie10 {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        System.out.println("Podaj swoj dochod: ");
        double dochod = scanner.nextDouble();
        double nadwyzka;

        if (dochod >= 85528.0D) {
            nadwyzka = dochod - 85528.0D;
            double podatek = 14839.02D + 0.32D * nadwyzka;
            System.out.println("Podatek = " + podatek);
        } else {
            double podatek = dochod * 0.18D - 556.02D;
            System.out.println("Podatek = " + podatek);
        }
    }
}