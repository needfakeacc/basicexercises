package zadanie24;

public class Zadanie24 {
    /** Napisać program, który:
     • utworzy tablicę 10 liczb całkowitych i wypełni ją wartościami losowymi z
     przedziału [−10, . . . , 10],
     • wypisze na ekranie zawartość tablicy,
     • wyznaczy najmniejszy oraz najwięszy element w tablicy,
     • wyznaczy średnią arytmetyczną elementów tablicy,
     • wyznaczy ile elementów jest mniejszych, ile większych od średniej.
     • wypisze na ekranie zawartość tablicy w odwrotnej kolejności, tj. od ostatniego
     do pierwszego.

     Wszystkie wyznaczone wartości powinny zostać wyświetlone na ekranie.
     Wylosowane liczby:
     -3 9 2 -10 -3 -4 -1 -5 -10 8
     Min: -10, max: 9
     Średnia: -1,00
     Mniejszych od śr.: 6
     Większych od śr.: 3
     Liczby w odwrotnej kolejności:
     8 -10 -5 -1 -4 -3 -10 2 9 -3*/
}
