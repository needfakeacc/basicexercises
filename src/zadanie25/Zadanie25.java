package zadanie25;

import java.util.Random;

public class Zadanie25 {
    public static void main(String[] args) {

        Random losowyCyfer = new Random();
        int iloscLiczb = 20;
        int minPrzedzial = 1;
        int maxPrzedzial = 10;

        int dlugoscPrzedzialu = maxPrzedzial - minPrzedzial;
        int[] tablica = new int[iloscLiczb];
        for (int i = 0; i < tablica.length ; i++) {
            tablica[i] = losowyCyfer.nextInt(dlugoscPrzedzialu+1)+minPrzedzial;
        }
        for (int i = 0; i < tablica.length ; i++) {
            System.out.print(tablica[i] + ", ");
        }
        int[] wystąpienia = new int [maxPrzedzial +1];

        for (int i = 0; i < tablica.length; i++) {
            int  liczba = tablica[i];
            wystąpienia[liczba]++;
        }
        for (int i = 0; i < wystąpienia.length ; i++) {
            System.out.println(i + " - " + wystąpienia[i]);
        }
    }
}
/**
 * Napisać program, który utworzy tablicę 20 liczb całkowitych z przedziału 1 . . . 10,
 * a następnie wypisze na ekranie ile razy każda z liczb z tego przedziału powtarza
 * się w tablicy.
 * Przykład:
 * Wylosowane liczby: 6 5 4 5 10 5 8 3 10 6 6 6 4 3 2 8 1 3 4 7
 * Wystąpienia:
 * 1 - 1
 * 2 - 1
 * 3 - 3
 * 4 - 3
 * 5 - 3
 * 6 - 4
 * 7 - 1
 * 8 - 2
 * 9 - 0
 * 10 - 2
 */
