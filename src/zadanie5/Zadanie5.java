package zadanie5;

public class Zadanie5 {
    public static void main(String[] args) {

        int waga = 50;
        int wzrost = 200;
        int wiek = 25;

        if(wiek < 10 || wiek > 80 ) {
            System.out.println("Nie mozesz wejsc z powodu wieku");
        }else if(wzrost > 220 || wzrost < 150) {
            System.out.println("Nie mozesz wejsc z powodu wzrostu");
        }else if(waga > 180) {
            System.out.println("Nie mozesz wejsc z powodu wagi");
        }else{
            System.out.println("Mozesz wejsc");
        }
    }
}
