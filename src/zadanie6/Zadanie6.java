package zadanie6;

public class Zadanie6 {
    public static void main(String[] args) {

        int ocena_matematyka = 3;
        int ocena_chemia = 2;
        int ocena_j_polski = 6;
        int ocena_j_angielski = 5;
        int ocena_wos = 3;
        int ocena_informatyka = 4;

        double sredniaCalkowita = (ocena_matematyka + ocena_chemia + ocena_j_polski +
                ocena_j_angielski + ocena_wos + ocena_informatyka) /6;

        double sredniaScisla = (ocena_matematyka + ocena_chemia + ocena_informatyka) /3;

        double sredniaHumanistyczna = (ocena_j_polski + ocena_j_angielski + ocena_wos) /3;

        System.out.println("Srednia wszystkich przedmiotow: " + sredniaCalkowita);
        System.out.println("Srednia przedmiotow scislych: " + sredniaScisla);
        System.out.println("Srednia przedmiotow humanistycznych: " + sredniaHumanistyczna);

        if(ocena_matematyka == 1) {
            System.out.println("Ocena z matematyki jest niedostateczna.");
        }
        if(ocena_chemia == 1) {
            System.out.println("Ocena z chemii jest niedostateczna.");
        }
        if(ocena_j_polski == 1) {
            System.out.println("Ocena z jezyka polskiego jest niedostateczna.");
        }
        if(ocena_j_angielski == 1) {
            System.out.println("Ocena z jezyka angielskiego jest niedostateczna.");
        }
        if(ocena_wos == 1) {
            System.out.println("Ocena z wos jest niedostateczna.");
        }
        if(ocena_informatyka == 1) {
            System.out.println("Ocena z informatyki jest niedostateczna.");
        }
        if(sredniaCalkowita == 1) {
            System.out.println("Srednia wszystkich ocen jest niedostateczna.");
        }
        if(sredniaScisla == 1) {
            System.out.println("Srednia ocen z przedmiotow scislych jest niedostateczna.");
        }
        if(sredniaHumanistyczna == 1) {
            System.out.println("Srednia ocen z przedmiotow humanistycznych jest niedostateczna.");
        }
    }
}
