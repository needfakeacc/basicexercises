package zadanie17;

import java.util.Scanner;

public class Zadanie17 {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        System.out.println("Podaj gorna granice: ");
        int liczbaCalkowita = sc.nextInt();

        double wynikPotegowania = 1.0;
        int i = 1;

        do {
            System.out.println(wynikPotegowania);
            wynikPotegowania = Math.pow(2, i++);
        } while(wynikPotegowania < liczbaCalkowita);


        double wynik = 1.0;
        do {
            System.out.println(wynik);
            wynik = wynik * 2;
        } while (wynik < liczbaCalkowita);
    }
}
