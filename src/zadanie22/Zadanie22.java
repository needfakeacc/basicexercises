package zadanie22;

import java.util.Scanner;

public class Zadanie22 {
    public static void main(String[] args) {
        Scanner skanuje = new Scanner(System.in);
        System.out.println("Podaj liczbe do podzielenia: ");
        int number = skanuje.nextInt();

        System.out.println("Liczba " + number + ", jest podzielna przez: ");
        for (int i = 1; i <= number; i++) {
            if (number % i == 0) {
                System.out.print(i + " ");
            }
        }
    }
}
