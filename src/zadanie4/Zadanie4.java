package zadanie4;

public class Zadanie4 {
    public static void main(String[] args) {

        boolean jest_cieplo = true;
        boolean wieje_wiatr = true;
        boolean swieci_slonce = true;

        boolean ubieram_sie_cieplo = false;
        boolean biore_parasol = false;
        boolean ubieram_kurtke = false;

        if (jest_cieplo == false || wieje_wiatr == true) {
            ubieram_sie_cieplo = true;
            System.out.println("ubieram sie cieplo = " + ubieram_sie_cieplo);
        }
        if (swieci_slonce == false && wieje_wiatr == false) {
            biore_parasol = true;
            System.out.println("biore parasol = " + biore_parasol);
        }
        if (wieje_wiatr == true && swieci_slonce == false && jest_cieplo == false) {
            ubieram_kurtke = true;
            System.out.println("ubieram kurtke = " + ubieram_kurtke);
        }
    }
}
