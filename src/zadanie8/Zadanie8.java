package zadanie8;

public class Zadanie8 {
    public static void main(String[] args) {

        int a = 24;
        int b = 3;
        int c = 157;

        System.out.println(a + "   " + b + "   " + c);
        System.out.println();

        int[] tab = new int[]{a, b, c};
        int min = a;
        int max = a;

        for (int i = 0; i < tab.length; i++) {
            if (min > tab[i]) {
                min = tab[i];
            }
            if (max < tab[i]) {
                max = tab[i];
            }
        }
        System.out.println("Minimum : " + min);
        System.out.println("Maximum : " + max);
        }
    }