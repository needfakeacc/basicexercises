package zadanie20;

import java.util.Random;
import java.util.Scanner;

public class Zadanie20 {
    public static void main(String[] args) {
        Random generator = new Random();
        Scanner skanuj = new Scanner(System.in);

        int number;
        int random = generator.nextInt(100)+1;

        System.out.println("Zgadnij liczbałkę:");

        while ((number = skanuj.nextInt()) != random) {
            if (number > random) {
                System.out.println("Podałeś za dużą wartość ziomuś!");
            } else {
                System.out.println("Podałeś za małą wartość hommie!");
            }
        }
        System.out.println("DAAAAAAMN SON!");
        System.out.println("Gratulacje, wygrałeś!");
        System.out.println("###GAME OVER###");
    }
}
