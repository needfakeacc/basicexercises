package zadanie9;

import java.util.Scanner;

public class zadanie9 {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        System.out.println("Podaj wage: ");//komunikat
        int waga = scanner.nextInt();//skan

        System.out.println("Podaj wzrost: ");
        int wzrost = scanner.nextInt();

        System.out.println("Podaj wiek: ");
        int wiek = scanner.nextInt();

        if (wiek < 10 || wiek > 80) {
            System.out.println("Nie mozesz wejsc z powodu wieku");
        } else if (wzrost > 220 || wzrost < 150) {
            System.out.println("Nie mozesz wejsc z powodu wzrostu");
        } else if (waga > 180) {
            System.out.println("Nie mozesz wejsc z powodu wagi");
        } else {
            System.out.println("Mozesz wejsc");
        }
    }
}

